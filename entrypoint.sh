#!/usr/bin/env bash

set -eo pipefail

[[ "${CMG_EP_TRACE}" =~ ^1|[Tt]rue$ ]] && \
  set -x

# constants
CMG_BASE_DIR=$(dirname $(readlink -f $0))
CMG_MAX_JITTER_DELAY="${CMG_MAX_JITTER_DELAY:-"3"}"
CMG_OUT_METRICS_DIR="${CMG_OUT_METRICS_DIR:-"${CMG_BASE_DIR}/out-metrics-dir"}"
CMG_STDOUT_LOG="${CMG_STDOUT_LOG:-"${CMG_BASE_DIR}/custom-metrics-generator.stdout.log"}"
CMG_STDERR_LOG="${CMG_STDERR_LOG:-"${CMG_BASE_DIR}/custom-metrics-generator.stderr.log"}"
CMG_EXEC_MODULES="${CMG_EXEC_MODULES:-"smartmon dockermon puppetmon noderolemon gatewaypingmon gpumon diskmon"}"
CMG_NODEROLEMON_METRICS_TEXTFILE=${CMG_NODEROLEMON_METRICS_TEXTFILE:-"/etc/node-exporter/node-role.prom"}

# load the library
source ${CMG_BASE_DIR}/src/lib.sh

# create logs if missing and rotate them
touch ${CMG_STDOUT_LOG} ${CMG_STDERR_LOG}
rotate_file ${CMG_STDOUT_LOG} &
rotate_file ${CMG_STDERR_LOG} &

# create out directory (if not present)
test -d "${CMG_OUT_METRICS_DIR}" || \
  mkdir -p "${CMG_OUT_METRICS_DIR}"

i_fixed_sleep=0
echo -e "SHELL=/bin/bash\nPATH=/usr/bin:/bin:/usr/sbin\n\n" > /var/spool/cron/root
for i_module in ${CMG_EXEC_MODULES}; do
    touch "${CMG_OUT_METRICS_DIR}/${i_module}.prom" \
      "${CMG_OUT_METRICS_DIR}/custom-metrics-generator_${i_module}.prom" > /dev/null
    echo "* * * * * ${CMG_BASE_DIR}/src/delay-jitter-exec.sh ${i_fixed_sleep} ${CMG_MAX_JITTER_DELAY} \
      ${CMG_BASE_DIR}/src/metric-generator-exec.sh ${i_module} ${CMG_OUT_METRICS_DIR}/${i_module}.prom ${CMG_OUT_METRICS_DIR}/custom-metrics-generator_${i_module}.prom"
    i_fixed_sleep=$(( ${i_fixed_sleep} + 5 ))
done >> /var/spool/cron/root

echo "custom-metrics-generator configuration:"
set | grep -E '^CMG_' | awk '{print "export " $0}' > "${CMG_BASE_DIR}/custom-metrics-generator.conf.env"
awk '{$1=" ";print}' "${CMG_BASE_DIR}/custom-metrics-generator.conf.env"

# attach log files to container's stdout and stderr
tail -F --max-unchanged-stats=10 ${CMG_STDOUT_LOG} &
tail -F --max-unchanged-stats=10 ${CMG_STDERR_LOG} 1>&2 &

# Make env accessible to cron jobs (enables cron job config)
env >> /etc/environment

# schedule periodic actions with cron
exec crond -n
