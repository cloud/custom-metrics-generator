#!/usr/bin/env bash

# prometheus textfile metrics generator for node-exporter textfile collector
# Monitors: gateway reachability from host
# Usage: gatewaypingmon.sh
# Optional: GATEWAYPINGMON_GATEWAYS to set a list of network gateways
# Optional: GATEWAYPINGMON_PING_ARGS to set arguments for ping command

set -eo pipefail

METRIC_NAME="gatewaypingmon_prefix_gateway_accessible"
if [ -z "${GATEWAYPINGMON_GATEWAYS}" ] ; then
  if hostname | grep -qF .stage.; then
    GATEWAYPINGMON_GATEWAYS=("10.16.113.1" "10.16.104.1" "10.16.127.129" "147.251.62.129")
  else
    GATEWAYPINGMON_GATEWAYS=("10.16.100.1" "10.16.108.1" "10.16.61.1" "147.251.62.129")
  fi
else
  if ! declare -p GATEWAYPINGMON_GATEWAYS | grep -qE "declare[ \t]+-a[ \t]+"; then
    GATEWAYPINGMON_GATEWAYS=(${GATEWAYPINGMON_GATEWAYS})
  fi
fi
GATEWAYPINGMON_PING_ARGS=${GATEWAYPINGMON_PING_ARGS:-"-c 4 -i 0.2"}
ACCESS_RATIO=()
CMG_SRC_DIR=$(dirname $(dirname $(readlink -f $0)))

STAGE_NAME="configuration loaded"
source "${CMG_SRC_DIR}/../custom-metrics-generator.conf.env"

STAGE_NAME="library loaded"
source "${CMG_SRC_DIR}/lib.sh"

STAGE_NAME="accessibility of mandatory gateways from host tested"
for gateway in ${GATEWAYPINGMON_GATEWAYS[@]}; do
  ping_result=$(ping ${GATEWAYPINGMON_PING_ARGS} ${gateway} || echo "0 received, 100% packet loss")
  packet_loss_percent=$(echo $ping_result | sed -n -e 's/^.*, \(.*\)% packet.*/\1/p')
  ACCESS_RATIO+=($(echo "scale=2 ; 1 - ${packet_loss_percent} / 100" | bc))
done

STAGE_NAME="accessibility of gateways is generated"
printf '# HELP %s: Current network gateway ping access ratio. (1 ~ no loss, 0 ~ complete loss)\n' "${METRIC_NAME}"
for ((i=0; i<${#GATEWAYPINGMON_GATEWAYS[*]}; i++)); do
  printf '%s{gateway="%s"} %.2f\n' \
    "${METRIC_NAME}" "${GATEWAYPINGMON_GATEWAYS[${i}]}" "${ACCESS_RATIO[${i}]}"
done
