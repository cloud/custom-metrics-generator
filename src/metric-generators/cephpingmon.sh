#!/bin/bash

METRIC_NAME="cephpingmon_endpoint_accessible"
if [[ -z "${CEPH_IPS}" ]]; then
  CEPH_IPS="147.251.62.133 147.251.62.134 147.251.62.135 10.16.80.1"
fi
if [[ -z "${MTU_SIZE}" ]]; then
  MTU_SIZE=8972
fi

function publish_metrics_nofrag {
  local ping_result
  local loss_percent
  local success_ratio
  local fragmentation="no"

  for ip in $CEPH_IPS; do
    ping_result=$(ping -q -c4 -W 1 -s $MTU_SIZE -M do $ip)
    loss_percent=$(echo ${ping_result} | sed -n -e 's/^.*, \(.*\)% packet.*/\1/p')
    success_ratio=$(echo "scale=2 ; 1 - ${loss_percent} / 100" | bc)
    LC_NUMERIC=C printf '%s{endpoint_ip="%s", fragmentation="%s"} %.2f\n' \
      "${METRIC_NAME}" $ip $fragmentation "${success_ratio}"
  done
}

function publish_metrics_frag {
  local ip
  local loss_percent
  local success_ratio
  local fragmentation="yes"

  while IFS= read -r line; do
    ip=$(echo $line | cut -d' ' -f1)
    loss_percent=$(echo $line | cut -d' ' -f5 | cut -d'/' -f3 | tr -d % | tr -d ,)
    success_ratio=$(echo "scale=2 ; 1 - ${loss_percent} / 100" | bc)
    LC_NUMERIC=C printf '%s{endpoint_ip="%s", fragmentation="%s"} %.2f\n' \
      "${METRIC_NAME}" $ip $fragmentation "${success_ratio}"
  done <<< "$1"
}

printf '# HELP %s: Ceph endpoint ping success ratio. (1 ~ no loss, 0 ~ complete loss)\n' $METRIC_NAME
ping_result=$(fping -q -c4 -b $MTU_SIZE $CEPH_IPS 2>&1)
publish_metrics_frag "${ping_result}"
publish_metrics_nofrag
