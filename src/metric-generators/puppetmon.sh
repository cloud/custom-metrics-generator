#!/usr/bin/env bash

# prometheus textfile metrics generator for node-exporter textfile collector
# Monitors: puppet host management state
# https://puppet.com/blog/puppet-monitoring-how-to-monitor-success-or-failure-of-puppet-runs/
#
# Usage: puppetmon.sh <last-known-generated-metric-file>

set -eo pipefail

# constants
PUPPET_STATE_FILE="/var/lib/puppet/state/last_run_summary.yaml"
YQ="/usr/local/bin/yq"
CMG_SRC_DIR=$(dirname $(dirname $(readlink -f $0)))
PUPPETMON_GENERATED_METRIC_FILE="$1"

STAGE_NAME="configuration loaded"
source "${CMG_SRC_DIR}/../custom-metrics-generator.conf.env"

STAGE_NAME="library loaded"
source "${CMG_SRC_DIR}/lib.sh"

STAGE_NAME="puppet state file is valid"
puppet_state_file_valid=0
${YQ} eval . "${PUPPET_STATE_FILE}" > /dev/null && \
  puppet_state_file_valid=1

puppet_version=$(${YQ} eval ".version.puppet" "${PUPPET_STATE_FILE}")
echo -n "${puppet_version}" | grep -Eq "^[0-9]+\.[0-9]+\.[0-9]+"
puppet_time_last_run=$(${YQ} eval ".time.last_run" "${PUPPET_STATE_FILE}")
echo -n "${puppet_time_last_run}" | grep -Eq "^[0-9]+$"
puppet_events_failure_count=$(${YQ} eval ".events.failure" "${PUPPET_STATE_FILE}")
echo -n "${puppet_events_failure_count}" | grep -Eq "^[0-9]+$"

STAGE_NAME="puppet state file vality metric is generated"
METRIC_NAME="puppetmon_puppet_statefile_valid"
get_metric_help "${METRIC_NAME}" "gauge" "Puppet state file validity flag."
printf '%s %d\n' "${METRIC_NAME}" "${puppet_state_file_valid}"

STAGE_NAME="puppet version metric is generated"
METRIC_NAME="puppetmon_puppet_version"
get_metric_help "${METRIC_NAME}" "gauge" "Puppet version."
printf '%s{version="%s"} 1\n' "${METRIC_NAME}" "${puppet_version}"

STAGE_NAME="puppet last run timestamp metric is generated"
METRIC_NAME="job_last_run_timestamp"
get_metric_help "${METRIC_NAME}" "gauge" "Last job execution timestamp."
printf '%s{app="puppetmon"} %d\n' "${METRIC_NAME}" "${puppet_time_last_run}"

STAGE_NAME="puppet last successful run timestamp metric is generated"
METRIC_NAME="job_last_successful_run_timestamp"
get_metric_help "${METRIC_NAME}" "gauge" "Last successful job execution timestamp."
if [ "${puppet_events_failure_count}" == "0" ]; then
    printf '%s{app="puppetmon"} %d\n' "${METRIC_NAME}" "${puppet_time_last_run}"
else
    if grep -q "^${METRIC_NAME}" "${PUPPETMON_GENERATED_METRIC_FILE}"; then
        printf '%s{app="puppetmon"} %d\n' "${METRIC_NAME}" "$(grep "^${METRIC_NAME}" "${PUPPETMON_GENERATED_METRIC_FILE}" | head -1 | awk '{print $NF}')"
    else
        printf '%s{app="puppetmon"} %d\n' "${METRIC_NAME}" "0"
    fi
fi
