#!/bin/bash

set -eo pipefail

DEVICES="$(lsblk -o name,rota --raw --noheadings)"
METRIC_NAME="diskmon_is_rotational"

printf '# HELP %s Information about disk being rotational.\n' $METRIC_NAME
printf '# TYPE %s gauge\n' $METRIC_NAME

while read device; do
    name=$(echo $device | cut -d ' ' -f1)

    if ls /dev | grep -q $name; then
        name="/dev/${name}"
    elif [[ "$name" == "rootfs" ]] || [[ "$name" == "tmpfs" ]]; then
        : # name is correct
    else
        name="/dev/mapper/${name}"
    fi

    printf '%s{device="%s"} %d\n' $METRIC_NAME $name "$(echo $device | cut -d ' ' -f2)"
done <<< "$DEVICES"
