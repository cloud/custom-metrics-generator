#!/usr/bin/env bash

# prometheus textfile metrics generator for node-exporter textfile collector
# Monitors: docker container restarts
# cAdvisor does not provide container number of restarts nicely (conditionally in label only)

set -eo pipefail

METRIC_NAME="dockermon_container_restarts_total"
CONTAINER_IDS=()
CONTAINER_NAMES=()
CONTAINER_RESTART_COUNTS=()

CONTAINERS=$(docker ps -a -q)

for i_container in ${CONTAINERS}; do
  i_container_status="$(docker inspect "${i_container}")"
  i_container_name="$(echo "${i_container_status}" | jq -r '.[].Name' | sed 's|^/||')"
  i_container_restart_count="$(echo "${i_container_status}" | jq -r '.[].RestartCount')"
  i_container_id="/docker/$(echo "${i_container_status}" | jq -r '.[].Id')"
  test -n "${i_container_name}"
  echo -n "${i_container_restart_count}" | grep -Eq "^[0-9]+$"
  echo -n "${i_container_id}" | grep -Eq "^/docker/[a-fA-F0-9]{12,}$"
  CONTAINER_IDS+=("${i_container_id}")
  CONTAINER_NAMES+=("${i_container_name}")
  CONTAINER_RESTART_COUNTS+=("${i_container_restart_count}")
done

printf '# HELP %s Number of container restarts.\n' "${METRIC_NAME}"
printf '# TYPE %s counter\n' "${METRIC_NAME}"
for ((indx=0; indx<${#CONTAINER_IDS[*]}; indx++)); do
  printf '%s{id="%s",name="%s"} %d\n' "${METRIC_NAME}" "${CONTAINER_IDS[${indx}]}" \
    "${CONTAINER_NAMES[${indx}]}" "${CONTAINER_RESTART_COUNTS[${indx}]}"
done
