
[[ "${CMG_TRACE}" =~ ^1|[Tt]rue$ ]] && \
  set -x


# get_metric_help ( <metric-name> <metric-type> <metric-desc>)
#   generates testform metric help
function get_metric_help() {
  local metric_name="$1"
  local metric_type="$2"
  local metric_desc="$3"

  echo "# HELP ${metric_name} ${metric_desc}"
  echo "# TYPE ${metric_name} ${metric_type}"
}

# rotate_file ( <log-path> [file-max-size] )
#   rotate file  <log-path>
function rotate_file() {
    local file_path="$1"
    local max_file_size="${2:-$((10 *1024 * 1024))}"
    while true; do
        local i_time="$(date +%Y%m%dT%H%M%S)"
        local i_file_size=$(stat --format=%s "${file_path}")
        if (( ${i_file_size} > ${max_file_size} )); then
            rm -f $1.rotated.*
            cat "${file_path}" > "${file_path}.rotated.${i_time}"
            truncate --size=0 "${file_path}"
            log_stdout "${file_path} rotated to ${file_path}.rotated.${i_time} (size was ${i_file_size} > ${max_file_size})"
        fi
        sleep 30
    done
}

# __log ( <file> <msg> [msg] ...)
#   logs message to stdout log file
function __log() {
    local log_file="$1"
    shift
    local msg="[$(date '+%Y-%m-%d %H:%M:%S.%N')] $@"
    if [ -f "${log_file}" ]; then
        echo "${msg}" >> "${log_file}"
    else
        echo "${msg}" 1>&2
    fi
}

# log_stdout ( <msg> [msg] ...)
#   logs message to stdout log file
function log_stdout() {
    __log "${CMG_STDOUT_LOG}" "$@"
}

# log_stderr ( <msg> [msg] ...)
#   logs message to stdout log file
function log_stderr() {
    __log "${CMG_STDERR_LOG}" "$@"
}
