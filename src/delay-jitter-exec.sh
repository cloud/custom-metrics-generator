#!/usr/bin/env bash
# delay-jitter-exec.sh
#
# Usage:
#   delay-jitter-exec.sh <fixed-delay-secs> <max-jitter-secs> <cmd> [cmd-arguments]
#
# Execute specified command <cmd> [cmd-arguments]
#   with the
#   * fixed delay <fixed-delay-secs>
#   * jitter delay <max-jitter-secs> < 0, <max-jitter-secs> >
#
# Examples:
#  # launch `smartmon.sh /dev/sda` after delay which is in range of <0.5 + 0, 0.5 + 1.15> seconds
#  $ jitter-exec.sh 0.5 1.15 smartmon.sh /dev/sda

fixed_delay_secs=${1:-0}
max_jitter_secs=${2:-0}
[[ "${fixed_delay_secs}" =~ ^[0-9]+\.?([0-9]+)?|$ ]] || \
  fixed_delay_secs=0
[[ "${max_jitter_secs}" =~ ^[0-9]+\.?([0-9]+)?|$ ]] || \
  max_jitter=0

[ "${fixed_delay_secs}" != "0" ] && \
  sleep "${fixed_delay_secs}"
[ "${max_jitter_secs}" != "0" ] && \
    awk -v "rnd=${RANDOM}" -v "max_jitter_secs=${max_jitter_secs}" \
      'BEGIN{system(sprintf("sleep %f", rnd * max_jitter_secs / 32767.0))}'

shift 2

# execute <cmd>
"$@"

