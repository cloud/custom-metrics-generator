FROM registry.gitlab.ics.muni.cz:443/cloud/container-registry/centos:7

ARG VERSION=unknown-version
ARG BUILD_DATE=unknown-date
ARG CI_COMMIT_SHA=unknown
ARG CI_BUILD_HOSTNAME
ARG CI_BUILD_JOB_NAME
ARG CI_BUILD_ID
ARG YQ_BINARY_BASEURL="https://github.com/mikefarah/yq/releases/download/v4.9.6"
ARG YQ_BINARY_URL="${YQ_BINARY_BASEURL}/yq_linux_amd64"
ARG YQ_CHECKSUMS_URL="${YQ_BINARY_BASEURL}/checksums"

COPY dependencies.yum.txt /tmp

RUN yum -y install epel-release && \
    yum -y update && \
    yum -y install $(cat /tmp/dependencies.yum.txt) && \
    curl -L "${YQ_BINARY_URL}" --output "/tmp/$(basename "${YQ_BINARY_URL}")" && \
    curl -L "${YQ_CHECKSUMS_URL}" --output "/tmp/$(basename "${YQ_CHECKSUMS_URL}")" && \
    bash -xc 'cd /tmp ; f="$(basename ${YQ_BINARY_URL})" ; chsum="$(sha256sum "${f}" | awk "{print \$1}")" ; grep -F "${f}" checksums | grep -F " ${chsum}"' && \
    mv -f "/tmp/$(basename "${YQ_BINARY_URL}")" /usr/local/bin/yq && \
    chmod +x /usr/local/bin/yq && \
    rm -f "/tmp/$(basename "${YQ_CHECKSUMS_URL}")" "/tmp/dependencies.yum.txt" && \
    yum clean all && \
    mkdir -p /opt/custom-metrics-generator

ADD custom-metrics-generator_files.tgz /opt/custom-metrics-generator/

ENTRYPOINT ["/opt/custom-metrics-generator/entrypoint.sh"]

LABEL maintainer="MetaCentrum Cloud Team <cloud[at]ics.muni.cz>" \
      org.label-schema.schema-version="1.0.0-rc.1" \
      org.label-schema.vendor="Masaryk University, ICS" \
      org.label-schema.name="custom-metrics-generator" \
      org.label-schema.version="$VERSION" \
      org.label-schema.build-date="$BUILD_DATE" \
      org.label-schema.build-ci-job-name="$CI_BUILD_JOB_NAME" \
      org.label-schema.build-ci-build-id="$CI_BUILD_ID" \
      org.label-schema.build-ci-host-name="$CI_BUILD_HOSTNAME" \
      org.label-schema.url="https://gitlab.ics.muni.cz/cloud/custom-metrics-generator" \
      org.label-schema.vcs-url="https://gitlab.ics.muni.cz/cloud/custom-metrics-generator" \
      org.label-schema.vcs-ref="$CI_COMMIT_SHA"
