# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.3.4] - 2024-03-25
### Added
- cephpingmon added as optional module (default behavior unchanged), add fping dependency

## [1.3.3] - 2023-10-25
### Fixed
- Env (vars) are made accessible to cronjobs

## [1.3.2] - 2023-09-05
### Fixed
- diskmon: change metric format

## [1.3.1] - 2023-09-04
### Fixed
- diskmon: use by default

## [1.3.0] - 2023-08-30
### Added
- diskmon: new monitor to gather disk information not present elsewhere

## [1.2.5] - 2022-06-07
### Fixed
- cron: assure lspci is executable
- metric-generator-exec.sh: better logging/tracing with CMG_TRACE

## [1.2.4] - 2022-06-07
### Fixed
- gpumon: GPU_DEVICES_UNRECOGNIZED detection reworked without grep, with gawk, avoiding crash

## [1.2.3] - 2022-06-07
### Fixed
- gpumon: publish at least a metric help (metric-generator-exec requirement)
- metric-generator-exec.sh: added note on how should generator module behave

## [1.2.2] - 2022-06-07
### Fixed
- gpumon: avoid crashing on no GPU (readlink)

## [1.2.1] - 2022-06-07
### Fixed
- gpumon: avoid crashing on no GPU
- gpumon: avoid reporting GPU metric help on no GPU

## [1.2.0] - 2022-06-07
### Added
- GPU presence and availablility added as gpumon metric generator

## [1.1.1] - 2022-05-03
### Fixed
- Fix chmod for gatewaypingmon

## [1.1.0] - 2022-04-26
### Added
- Reachability of controlplane gateways

## [1.0.5] - 2021-10-14
### Changed
- none, simple rebuild

## [1.0.4] - 2021-07-13
### Fixed
- dockermon id assert corrected, now expects `/docker/` prefix

## [1.0.3] - 2021-07-12
### Fixed
- compliance with cAdvisor:
  * container name (`name`) never starts with `/`, dropping leading slash too
  * container id (`id`) is prefixed with `/docker/`

## [1.0.2] - 2021-06-30
### Fixed
- dockermon textfile metrics documentation string now correctly prefixed with `# `

## [1.0.1] - 2021-06-30
### Changed
- change default output metric directory (singular->plural)
### Added
- Documentation added

## [1.0.0] - 2021-06-29
### Added
- Initial release
